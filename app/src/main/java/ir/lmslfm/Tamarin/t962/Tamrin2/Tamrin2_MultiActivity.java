package ir.lmslfm.Tamarin.t962.Tamrin2;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ir.lmslfm.Tamarin.t962.R;

public class Tamrin2_MultiActivity extends AppCompatActivity {
    EditText edt;
    Button btn;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tamrin2_multi);
        initViews();
        btn.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (edt.getText().toString().isEmpty()) {
                txt.setText("لطفا عدد را وارد کنید :| ");
            } else {
                int n = Integer.parseInt(edt.getText().toString());
                int t;
                String s[] = new String[n];

                String sx = new String();
                for (int i = 1; i < n + 1; i++) {
                    for (int j = 1; j < n + 1; j++) {
                        t = i * j;
                        if (j == 1) {
                            s[i - 1] = " "+String.valueOf(t);
                        } else {
                            s[i - 1] += "  " + String.valueOf(t);
                        }
                    }
                    sx += s[i - 1] + '\n';

                }

                txt.setText(sx);
            }
        }
    };


    public void initViews() {
        try {
            edt = (EditText) findViewById(R.id.activity_tamrin2_multi_edt);
            btn = (Button) findViewById(R.id.activity_tamrin2_multi_btn);
            txt = (TextView) findViewById(R.id.activity_tamrin2_multi_txt);
            Typeface iranSance = Typeface.createFromAsset(getAssets(),
                    "fonts/iran_sans_fa_num.ttf");
            txt.setTypeface(iranSance);
            txt.setTextSize(txt.getTextSize()-1);
            edt.setTypeface(iranSance);
            btn.setTypeface(iranSance);
        } catch (Exception e) {
            Toast.makeText(Tamrin2_MultiActivity.this, "Unkhnown Error . ", Toast.LENGTH_LONG).show();

        }
    }
}
