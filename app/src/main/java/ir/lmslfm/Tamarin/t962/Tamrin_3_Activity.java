package ir.lmslfm.Tamarin.t962;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Tamrin_3_Activity extends AppCompatActivity {
    EditText editText;
    Button btnSubmmit;
    Button btnFinish;
    TextView textView;
    int scores[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tamrin_3);
        initviews();
        btnSubmmit.setOnClickListener(onClickListenerNext);
        btnFinish.setOnClickListener(onClickListener);
        editText.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    // Perform action on Enter key press
                    btnSubmmit.callOnClick();

                    return true;
                }
                return false;

            }

        });

    }


    View.OnClickListener onClickListenerNext = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double f = 0;
            String fs = editText.getText().toString();

            try {
                f = Double.parseDouble(fs);
                scores[17]++;
                editText.setText("");
                textView.setText(String.valueOf(scores[14]));
            } catch (Exception e) {
               // Toast.makeText(Tamrin_3_Activity.this, "errrr", Toast.LENGTH_LONG).show();
            }
            if (f >= 6) {
                scores[16]++;
            }
            if (f >= 5.2) {
                scores[15]++;
            }
            if (f >= 4.6) {
                scores[14]++;
            }
            if (f >= 4) {
                scores[13]++;
            }
            if (f >= 3.4) {
                scores[12]++;
            }
            if (f >= 2.8) {
                scores[11]++;
            }
            if (f >= 2.4) {
                scores[10]++;
            }
            if (f >= 2) {
                scores[9]++;
            }
            if (f >= 1.9) {
                scores[8]++;
            }
            if (f >= 1.8) {
                scores[7]++;
            }
            if (f >= 1.7) {
                scores[6]++;
            }
            if (f >= 1.6) {
                scores[5]++;
            }
            if (f >= 1.5) {
                scores[4]++;
            }
            if (f >= 1.4) {
                scores[3]++;
            }
            if (f >= 1.3) {
                scores[2]++;
            }
            if (f >= 1.2) {
                scores[1]++;
            }
            if (f >= 1.1) {
                scores[0]++;
            }


        }
    };


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            String finaly =
                    " 1.1      " + String.valueOf(scores[0]) + "    Final cons   =  " + String.valueOf(1.1 * scores[0] - scores[14]) + '\n'
                            + " 1.2      " + String.valueOf(scores[1]) + "    Final cons   =  " + String.valueOf(1.2 * scores[11] - scores[14]) + '\n'
                            + " 1.3      " + String.valueOf(scores[2]) + "    Final cons   =  " + String.valueOf(1.3 * scores[2] - scores[14]) + '\n'
                            + " 1.4      " + String.valueOf(scores[3]) + "    Final cons   =  " + String.valueOf(1.4 * scores[3] - scores[14]) + '\n'
                            + " 1.5      " + String.valueOf(scores[4]) + "    Final cons   =  " + String.valueOf(1.5 * scores[4] - scores[14]) + '\n'
                            + " 1.6      " + String.valueOf(scores[5]) + "    Final cons   =  " + String.valueOf(1.6 * scores[5] - scores[14]) + '\n'
                            + " 1.7      " + String.valueOf(scores[6]) + "    Final cons   =  " + String.valueOf(1.7 * scores[6] - scores[14]) + '\n'
                            + " 1.8      " + String.valueOf(scores[7]) + "    Final cons   =  " + String.valueOf(1.8 * scores[7] - scores[14]) + '\n'
                            + " 1.9      " + String.valueOf(scores[8]) + "    Final cons   =  " + String.valueOf(1.9 * scores[8] - scores[14]) + '\n'
                            + " 2         " + String.valueOf(scores[9]) + "    Final cons   =  " + String.valueOf(2 * scores[9] - scores[14]) + '\n'
                            + " 2.4      " + String.valueOf(scores[10]) + "    Final cons   =  " + String.valueOf(2.4 * scores[10] - scores[14]) + '\n'
                            + " 2.8      " + String.valueOf(scores[11]) + "    Final cons   =  " + String.valueOf(2.8 * scores[11] - scores[14]) + '\n'
                            + " 3.4     " + String.valueOf(scores[12]) + "    Final cons   =  " + String.valueOf(3.4* scores[12] - scores[14]) + '\n'
                            + " 4         " + String.valueOf(scores[13]) + "    Final cons   =  " + String.valueOf(4 * scores[13] - scores[14]) + '\n'
                            + " 4.6       " + String.valueOf(scores[14]) + "    Final cons   =  " + String.valueOf(4 * scores[13] - scores[14]) + '\n'
                            + " 5.2        " + String.valueOf(scores[15]) + "    Final cons   =  " + String.valueOf(4 * scores[13] - scores[14]) + '\n'
                            + " 6         " + String.valueOf(scores[16]) + "    Final cons   =  " + String.valueOf(4 * scores[13] - scores[14]) + '\n'
                            + " From     " + String.valueOf(scores[17]) + "   Data ";
            textView.setText(finaly);

        }
    };

    public void initviews() {

        editText = (EditText) findViewById(R.id.activity_tamrin3_edt);
        btnSubmmit = (Button) findViewById(R.id.activity_tamrin3_btn_submit);
        btnFinish = (Button) findViewById(R.id.activity_tamrin3_btn_finish);
        textView = (TextView) findViewById(R.id.activity_tamrin3_txt);
        scores = new int[18];
        Typeface iranSance = Typeface.createFromAsset(getAssets(),
                "fonts/iran_sans_fa_num_bold.ttf");
        textView.setTypeface(iranSance);
    }
}
