package ir.lmslfm.Tamarin.t962;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ir.lmslfm.Tamarin.t962.Tamrin2.Tamrin_2_Activity;

public class MainActivity extends AppCompatActivity {


    TextView txtNameOfStd;
    TextView txtNumOfStd;
    Button btnTamrin2;
    Button btnTamrin3;
    Button btnTamrin4;
    Button btnTamrin5;
    Button btnTamrin6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnTamrin2 = (Button) findViewById(R.id.main_activity_btn_tamrin_2);
        btnTamrin2.setOnClickListener(tamrin2OnClickListener);
        getWindow().getDecorView().setBackgroundColor(MainActivity.this.getResources().getColor(R.color.back));
        initViews();

        btnTamrin3.setOnClickListener(tamrin3OnClickListener);
    }


    public void initViews() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Typeface iranSance = Typeface.createFromAsset(getAssets(),
                "fonts/iran_sans.ttf");
        btnTamrin2 = (Button) findViewById(R.id.main_activity_btn_tamrin_2);
        btnTamrin3 = (Button) findViewById(R.id.main_activity_btn_tamrin_3);
        btnTamrin4 = (Button) findViewById(R.id.main_activity_btn_tamrin_4);
        btnTamrin5 = (Button) findViewById(R.id.main_activity_btn_tamrin_5);
        txtNumOfStd = (TextView) findViewById(R.id.main_activity_txt_num_std);
        txtNameOfStd = (TextView) findViewById(R.id.main_activity_txt_name_std);

        View decorView = getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        btnTamrin2.setHeight(height * 2 / 10);
        btnTamrin3.setHeight(height * 2 / 10);
        btnTamrin4.setHeight(height * 2 / 10);
        btnTamrin5.setHeight(height * 2 / 10);
        btnTamrin2.setWidth(width * 5 / 8);
        btnTamrin3.setWidth(width * 3 / 8);
        btnTamrin4.setWidth(width * 4 / 10);
        btnTamrin5.setWidth(width * 6 / 10);
        btnTamrin5.setTypeface(iranSance);
        btnTamrin4.setTypeface(iranSance);
        btnTamrin3.setTypeface(iranSance);
        btnTamrin2.setTypeface(iranSance);

        txtNumOfStd.setTextSize(width/35);
        txtNameOfStd.setTextSize(width/25);
        txtNameOfStd.setTypeface(iranSance);
        txtNumOfStd.setTypeface(iranSance);

        btnTamrin2.setBackgroundColor(getResources().getColor(R.color.color00));
        btnTamrin3.setBackgroundColor(getResources().getColor(R.color.color01));
        btnTamrin4.setBackgroundColor(getResources().getColor(R.color.color02));
        btnTamrin5.setBackgroundColor(getResources().getColor(R.color.color03));


    }

    View.OnClickListener tamrin2OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent a = new Intent(MainActivity.this, Tamrin_2_Activity.class);
            startActivity(a);

        }
    };

    View.OnClickListener tamrin3OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent a = new Intent(MainActivity.this, Tamrin_3_Activity.class);
            startActivity(a);

        }
    };

}
