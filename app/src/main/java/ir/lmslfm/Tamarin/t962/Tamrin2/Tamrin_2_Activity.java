package ir.lmslfm.Tamarin.t962.Tamrin2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ir.lmslfm.Tamarin.t962.R;

public class Tamrin_2_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tamrin_2);
        Button btnStrings=(Button)findViewById(R.id.btn_tamrin2_activity_string);
        Button btnMulti=(Button)findViewById(R.id.btn_tamrin2_activity_multi);
        btnStrings.setOnClickListener(stringOnClickListener);
        Button PI=(Button)findViewById(R.id.btn_tamrin2_activity_pi);
        PI.setOnClickListener(piOnClickListener);
        btnMulti.setOnClickListener(multiOnClickListener);

    }







    View.OnClickListener piOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent  a=new Intent(Tamrin_2_Activity.this,Tamrin2_Pi.class);
            startActivity(a);
        }
    };

    View.OnClickListener stringOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent  a=new Intent(Tamrin_2_Activity.this,Tamrin2_Strings.class);
            startActivity(a);
        }
    };
View.OnClickListener multiOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent  a=new Intent(Tamrin_2_Activity.this,Tamrin2_MultiActivity.class);
            startActivity(a);
        }
    };
}
