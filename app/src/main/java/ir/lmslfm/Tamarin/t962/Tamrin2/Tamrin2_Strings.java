package ir.lmslfm.Tamarin.t962.Tamrin2;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ir.lmslfm.Tamarin.t962.R;

public class Tamrin2_Strings extends AppCompatActivity {

    TextView stringsEditText;
    TextView txtViewStringsDefine;
    EditText edtAsli;
    EditText edtTedad;
    Button btnSubmitStrings;
    Button btnSubmit;
    int counterOfPosts = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tamrin2_strings);
        initViews();

    }



    public void setViewsContent() {
        btnSubmitStrings.setText("SUBMIT " + counterOfPosts);
        txtViewStringsDefine.setText("TEXTVIEW OF " + counterOfPosts);

        counterOfPosts++;
    }


    public void initViews() {
        edtTedad = (EditText) findViewById(R.id.tamrin2_strings_edt_tedad);
        btnSubmit = (Button) findViewById(R.id.tamrin2_strings_btn_submit);
        edtAsli = (EditText) findViewById(R.id.dialogue_edt_t2);
        stringsEditText = (EditText)findViewById(R.id.tamrin2_strings_edt_string);
        txtViewStringsDefine = (TextView) findViewById(R.id.tamrin2_strings_txtview_str);
        btnSubmitStrings = (Button) findViewById(R.id.tamrin2_strings_btn_sub_strings);
    }


}
