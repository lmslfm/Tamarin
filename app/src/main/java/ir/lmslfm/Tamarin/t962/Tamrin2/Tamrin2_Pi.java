package ir.lmslfm.Tamarin.t962.Tamrin2;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ir.lmslfm.Tamarin.t962.R;

public class Tamrin2_Pi extends AppCompatActivity {
    Button btn;
    TextView numberName;
    TextView numberPi;
    TextView numberDegree;
    TextView sinusName;
    TextView sinusValue;
    TextView cosinusName;
    TextView cosinusValue;
    TextView tangantName;
    TextView tangantValue;
    TextView cotgName;
    TextView cotgValue;
    TextView alakiNumber;
    TextView alakiSinus;
    TextView alakiCosinus;
    TextView alakiTangant;
    TextView alakiCotg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tamrin2__pi);
        initViews();
        btn.setOnClickListener(onClickListener);
    }

    public void initViews() {
        Typeface iranSance = Typeface.createFromAsset(getAssets(),
                "fonts/iran_sans.ttf");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        numberName = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_rand_name);
        numberPi = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_rand_degree);
        numberDegree = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_rand_value);
        sinusName = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_sinus_name);
        sinusValue = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_sinus_value);
        cosinusName = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_cosinus_name);
        cosinusValue = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_cosinus_value);
        tangantName = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_tangant_name);
        tangantValue = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_tangant_value);
        cotgName = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_cotangant_name);
        cotgValue = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_cotangant_value);
        btn = (Button) findViewById(R.id.activity_tamrin2_pi_btn);

        numberDegree.setTypeface(iranSance);
        numberName.setTypeface(iranSance);
        numberPi.setTypeface(iranSance);
        sinusName.setTypeface(iranSance);
        sinusValue.setTypeface(iranSance);
        cosinusValue.setTypeface(iranSance);
        cosinusName.setTypeface(iranSance);
        tangantValue.setTypeface(iranSance);
        tangantName.setTypeface(iranSance);
        cotgName.setTypeface(iranSance);
        cotgValue.setTypeface(iranSance);
        btn.setTypeface(iranSance);

        alakiNumber = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_ar);
        alakiSinus = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_as);
        alakiCosinus = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_ac);
        alakiTangant = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_atg);
        alakiCotg = (TextView) findViewById(R.id.activity_tamrin2__pi_txt_acotg);


        alakiNumber.setWidth(width / 4);
        alakiSinus.setWidth(width / 4);
        alakiCosinus.setWidth(width / 4);
        alakiTangant.setWidth(width / 4);
        alakiCotg.setWidth(width / 4);
        alakiNumber.setHeight(height / 12);
        alakiSinus.setHeight(height / 12);
        alakiCosinus.setHeight(height / 12);
        alakiTangant.setHeight(height / 12);
        alakiCotg.setHeight(height / 12);
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            double npi = Math.random() * Math.PI;
            double nDegree = Math.toDegrees(npi);
            numberDegree.setText(String.valueOf((double) (((int) nDegree * 100)) / 100));
            numberPi.setText(String.valueOf((double) (((int) npi * 100)) / 100));


        }
    };
}
